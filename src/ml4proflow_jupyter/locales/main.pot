# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-11 22:41+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: main.py:97
msgid "Icon"
msgstr ""

#: main.py:97
msgid "Select"
msgstr ""

#: main.py:97 main.py:176
msgid "Name"
msgstr ""

#: main.py:98
msgid "Categories"
msgstr ""

#: main.py:98
msgid "Pkg"
msgstr ""

#: main.py:98
msgid "Tags"
msgstr ""

#: main.py:132
msgid "Dataflow"
msgstr ""

#: main.py:167 main.py:189 main.py:206
msgid "Abort"
msgstr ""

#: main.py:187 main.py:204
msgid "File"
msgstr ""

#: main.py:191
msgid "Save dataflow config:"
msgstr ""

#: main.py:208
msgid "Load dataflow config:"
msgstr ""

