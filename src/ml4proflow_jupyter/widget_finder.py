from __future__ import annotations
import importlib
from ml4proflow_jupyter import widgets
from ml4proflow_jupyter.exceptions import NoSuchWidget


def find_by_name(module_ident: str) -> type[widgets.Widgetable]:
    module_name, class_name = module_ident.rsplit('.', 1)
    # line below raises ModuleNotFoundError on error
    module_module = importlib.import_module(module_name)
    try:
        widget_class = getattr(module_module, class_name)
    except AttributeError:
        err_msg = ("There is no Widget ('%s') available, you may need" +
                   " to install an additional package") % module_ident
        raise NoSuchWidget(err_msg) from None
    if not issubclass(widget_class, widgets.Widgetable):
        raise NoSuchWidget("Error: %s" % module_ident)
    return widget_class
