from __future__ import annotations
from typing import Any
from ipywidgets import HBox, VBox, HTML, Button, Layout, Label, Text, GridBox, Box  # type: ignore  # noqa: E501
from ipyfilechooser import FileChooser
import json
import gettext
from os import path
from ml4proflow.modules import DataFlowGraph, DataFlowManager, BasicModule
from ml4proflow_jupyter.widget_finder import find_by_name as find_widget_by_name  # noqa: E501
from ml4proflow import module_finder


t = gettext.translation(path.splitext(path.basename(__file__))[0],
                        path.dirname(path.abspath(__file__))+"/locales")
_ = t.gettext

custom_css = """
<style>
.jupyter-widgets.widget-tab > .p-TabBar .p-TabBar-tab {
    flex: 0 1 200px
}
.widget-label { min-width: 20ex !important; }
/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 40px;
  height: 20px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.switch span {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.switch span:before {
  position: absolute;
  content: "";
  height: 13px;
  width: 13px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

.switch input:checked + span {
  background-color: #2196F3;
}

.switch input:focus + span {
  box-shadow: 0 0 1px #2196F3;
}

.switch input:checked + span:before {
  -webkit-transform: translateX(20px);
  -ms-transform: translateX(20px);
  transform: translateX(20px);
}

/* Rounded sliders */
.switch span {
  border-radius: 34px;
}

.switch span:before {
  border-radius: 50%;
}
</style>
"""


class ModuleSelectionWidget(VBox):
    def __init__(self, parent: "DFGWidget"):
        VBox.__init__(self, [])
        self.gen_container_modules()
        self.parent = parent

    def gen_container_modules(self) -> None:
        modules = module_finder.find_and_import_framework_modules()
        title_row = [Label(_("Select")), Label(_("Icon")), Label(_("Name")),
                     Label(_("Categories")), Label(_("Pkg")), Label(_("Tags"))]
        _modules = []
        for m in sorted(modules['basicmodules'], key=lambda a: a.get_module_name()):
            desc = m.get_module_desc()
            if 'jupyter-gui-cls' in desc:
                btn = Button(icon='check', layout=Layout(width="auto"))
                btn.module = m
                btn.on_click(self.on_select_module)
            else:
                btn = Button(icon='ban', layout=Layout(width="auto"))
            _modules.append(btn)
            _modules.append(Label(desc.get("icon", "")))
            _modules.append(Label(desc.get("name", m.get_module_name())))
            _modules.append(Label(",".join(desc.get("categories", ""))))
            _modules.append(Label(m.get_module_path()))
            _modules.append(Label(",".join(m.get_module_tags())))
        layout = Layout(grid_template_columns="repeat(6, auto)",
                        grid_gap="5px 20px",
                        height="400px")
        container = GridBox(title_row+_modules, layout=layout)
        self.children = (container,)

    def on_select_module(self, event: Button) -> None:
        self.selected_module = event.module
        self.parent.on_new_module_accept()

    def get_selected_module(self) -> BasicModule:
        return self.selected_module


class DFGWidget(DataFlowGraph, VBox):
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any] = {}):
        DataFlowGraph.__init__(self, dfm, config)
        self.widgets = []
        self.gui_title = Label(_("Dataflow"),
                               layout=Layout(display="flex",
                                             justify_content="center"))
        self.gui_add_module_button = Button(description="+",
                                            layout=Layout(width="fit-content"))
        self.gui_add_module_button.on_click(self.show_select_new_module_dialog)
        self.gui_modules = VBox([self.gui_add_module_button])
        VBox.__init__(self, [self.gui_title, self.gui_modules])
        for m in self.modules:
            self.add_new_widget_for_module(m)

    def _on_new_module_abort(self, button: Button = None) -> None:
        self.children = (self.children[0], self.gui_modules, )

    def on_new_module_accept(self, button: Button = None) -> None:
        module_cls = self.module_selector.get_selected_module()
        if not module_cls:
            return
        module_inst = module_cls(self.dfm, {})
        # hack? we should create a func to add modules to a graph?:
        self.modules.append(module_inst)
        self.add_new_widget_for_module(module_inst)

    def add_new_widget_for_module(self, module_inst: BasicModule) -> None:
        module_desc = module_inst.__class__.get_module_desc()
        widget_cls_ident = module_desc.get('jupyter-gui-cls')
        widget_cls = find_widget_by_name(widget_cls_ident)
        widget_inst = widget_cls(module_inst)
        self.widgets.append(widget_inst)
        self.gui_modules.children = self.gui_modules.children[:-1] + \
            (widget_inst.get_module_main_row_gui(),
             self.gui_modules.children[-1],)
        self.children = (self.children[0], self.gui_modules,)

    def show_select_new_module_dialog(self, button: Button = None) -> None:
        abort = Button(description=_("Abort"))
        abort.on_click(self._on_new_module_abort)
        self.module_selector = ModuleSelectionWidget(self)
        container = VBox([self.module_selector, HBox([abort])])
        self.children = (self.children[0], container, )


class DFGTitle(VBox):
    def __init__(self, dfg_c: "DFGContainer"):
        self.title = Text(value=_("Name"))
        self.title.add_class("interactive-label")
        self.save = Button(icon="save", layout=Layout(width="fit-content"))
        self.load = Button(icon="file-o", layout=Layout(width="fit-content"))
        self.dfg_c = dfg_c
        self.load.on_click(self.on_load_file_select_click)
        self.save.on_click(self.on_save_file_select_click)
        self.title_container = HBox([self.title, self.load, self.save])
        VBox.__init__(self, [self.title_container])

    def on_save_file_select_click(self, btn):
        self.save_chooser = FileChooser(select_desc=_('File'))
        self.save_chooser.register_callback(self.on_save_click)
        self.save_abort = Button(description=_("Abort"))
        self.save_abort.on_click(self.on_abort_click)
        self.children = (Label(_("Save dataflow config:")),
                         self.save_chooser,
                         self.save_abort)

    def on_save_click(self, chooser) -> None:
        with open(chooser.value, "w") as jf:
            json.dump(self.dfg_c.container_dfg.get_config(), jf)
        self.children = (self.title_container, )

    def on_abort_click(self, btn) -> None:
        self.children = (self.title_container,)

    def on_load_file_select_click(self, btn):
        self.load_chooser = FileChooser(select_desc=_('File'))
        self.load_chooser.register_callback(self.on_load_click)
        self.load_abort = Button(description=_("Abort"))
        self.load_abort.on_click(self.on_abort_click)
        self.children = (Label(_("Load dataflow config:")),
                         self.load_chooser,
                         self.load_abort)

    def on_load_click(self, chooser: FileChooser) -> None:
        with open(chooser.value) as jf:
            config = json.load(jf)
            self.dfg_c.reload_dfg(config)
        self.children = (self.title_container,)


class DFGContainer(VBox):
    def __init__(self) -> None:
        self.dfm = DataFlowManager()
        self.container_dfg = DFGWidget(self.dfm)
        self.container_title = DFGTitle(self)
        self.h_line = HTML(value="<hr>")
        super(VBox, self).__init__([self.container_title,
                                    self.h_line,
                                    self.container_dfg,
                                    ])
        self.layout = Layout(display="flex", border="solid 1px")

    def reload_dfg(self, config: dict[str, Any]) -> None:
        print('reloading: '+repr(config))
        self.container_dfg.shutdown()
        self.dfm = DataFlowManager()
        self.container_dfg = DFGWidget(self.dfm, config)
        self.children = (self.container_title,
                         self.h_line,
                         self.container_dfg,
                         )


class MainWindow(HBox):
    def __init__(self) -> None:
        self.gui_new_dfg_button = Button(description="+",
                                         layout=Layout(width="fit-content"))
        self.gui_new_dfg_button.on_click(self.on_new_dfg_button)
        super(HBox, self).__init__([self.gui_new_dfg_button])

    def on_new_dfg_button(self, button: Button) -> None:
        empty_dfg = DFGContainer()
        self.children: Box = self.children[:-1] + (empty_dfg,
                                                   self.children[-1], )
