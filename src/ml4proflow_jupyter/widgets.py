from __future__ import annotations
import json
import yaml
import traceback
import os
from os import path
import gettext
from typing import Any
from pandas import DataFrame  # noqa: F401
import qgrid
from ipywidgets import HBox, VBox, Label, IntText, FloatText, Text, Button, \
                       Layout, HTML, DOMWidget, Accordion, Output, Dropdown, \
                       BoundedIntText, BoundedFloatText, Checkbox, Tab, \
                       IntRangeSlider, FloatRangeSlider, Textarea \
                       # type: ignore  # noqa: E501
from ipyfilechooser import FileChooser
from ipylab import Panel, JupyterFrontEnd
from ml4proflow.modules import BasicModule, SourceModule, SinkModule, \
                               ExecutableModule
from ml4proflow.exceptions import InterfaceFunction, NoSuchChannel

# Localisation
t = gettext.translation(path.splitext(path.basename(__file__))[0],
                        path.dirname(path.abspath(__file__))+"/locales")
_ = t.gettext


class Widgetable():
    def __init__(self, module: BasicModule):
        self.module = module

    def get_module_main_row_gui(self) -> HBox:
        raise InterfaceFunction("")


type_2_str = {str: "string",
              int: "integer",
              float: "float",
              list: "list",
              dict: "dict",
              bool: "bool"}


class WidgetableWithSettings(Widgetable):
    def __init__(self, module: BasicModule):
        Widgetable.__init__(self, module)
        module_desc = self.module.get_module_desc()
        module_name = module_desc.get(_("name"), self.module.get_module_name())
        self.visible_module_name = module_name

    def get_module_main_row_gui(self) -> DOMWidget:
        module_name_label = Label(self.visible_module_name,
                                  layout=Layout(width="auto"))
        main_btn = Button(icon="cog",
                          layout=Layout(width="fit-content"))
        main_btn.on_click(self.on_module_open_main_gui_click)
        return HBox([module_name_label, main_btn])

    def _get_module_description_box(self) -> DOMWidget:
        raise InterfaceFunction("")

    def _get_module_settings_box(self) -> DOMWidget:
        raise InterfaceFunction("")

    def _get_module_execution_box(self) -> DOMWidget:
        raise InterfaceFunction("")

    def _get_module_push_box(self) -> DOMWidget:
        raise InterfaceFunction("")

    def _get_module_pull_box(self) -> DOMWidget:
        raise InterfaceFunction("")

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return []

    def get_module_main_gui(self) -> DOMWidget:
        title = HTML("<H1>%s</H1>" % self.visible_module_name)
        accordion_child = [self._get_module_description_box(),
                           self._get_module_settings_box()
                           ]
        accordion_titles = [_("Description"),
                            _("Settings"),
                            ]
        if isinstance(self.module, ExecutableModule):
            accordion_child.append(self._get_module_execution_box())
            accordion_titles.append(_("Execute"))
        if isinstance(self.module, SinkModule):
            accordion_child.append(self._get_module_pull_box())
            accordion_titles.append(_("Inputs"))
        if isinstance(self.module, SourceModule):
            accordion_child.append(self._get_module_push_box())
            accordion_titles.append(_("Outputs"))
        for t, b in self._get_additional_boxes():
            accordion_child.append(b)
            accordion_titles.append(t)
        # one accordion per gui box
        accordions = []
        for i, ac in enumerate(accordion_child):
            accordion = Accordion(children=[ac],
                                  titles=[accordion_titles[i]])
            # fix title for old ipywidgets version
            accordion.set_title(title=accordion_titles[i], index=0)
            accordions.append(accordion)
        main = VBox(children=[title]+accordions,
                    layout=Layout(display='flex', flex='none'))
        return main

    def on_module_open_main_gui_click(self, btn: Any) -> None:
        panel = Panel()
        panel.children = [self.get_module_main_gui()]
        panel.title.label = self.visible_module_name
        app = JupyterFrontEnd()
        app.shell.add(panel, 'main')


class BasicWidget(WidgetableWithSettings):
    def __init__(self, module: BasicModule):
        WidgetableWithSettings.__init__(self, module)

    def _get_module_description_box(self) -> DOMWidget:
        module_desc = self.module.get_module_desc()
        html_str = module_desc.get('html-description', '')
        return HTML(html_str)

    def _get_module_execution_box(self) -> DOMWidget:
        layout = Layout(display="flex",
                        height="15em",
                        flex_flow="column",
                        border="1px solid grey", padding="5px")
        self._execution_box_log = Output(layout=layout)
        execute_once = Button(description=_("Execute once"))

        def execute_once_wrapper(btn: Any) -> None:
            with self._execution_box_log:
                self.module.execute_once()
        execute_once.on_click(execute_once_wrapper)
        buttons = HBox(children=[execute_once])
        return VBox(children=[buttons, Label(_('Log:')), self._execution_box_log])

    def _get_settingwidget_by_string(self, typestr: str, value: Any,
                                     desc_label: str, desc_tooltip: str,
                                     config_key: str) -> DOMWidget:
        layout = Layout(width='13em')
        bounded = type(self.module.config_range.get(config_key, '')) == tuple
        if bounded:
            bound_tmp = self.module.config_range.get(config_key)
        # use a dropdown if the range setting is a list
        if type(self.module.config_range.get(config_key, '')) == list:
            widget = Dropdown(options=self.module.config_range.get(config_key),
                              value=value)
        elif typestr == "string":
            widget = Text(value=value)
        elif typestr == "bool":
            widget = Checkbox(value=value)
            desc_label = desc_label[0:-1]  # remove : because caption is on the right
        elif typestr == "integer" and not bounded:
            widget = IntText(value=value)
        elif typestr == "integer":
            widget = BoundedIntText(value=value, max=bound_tmp[1],
                                    min=bound_tmp[0], step=bound_tmp[2])
        elif typestr == "float" and not bounded:
            widget = FloatText(value=value)
        elif typestr == "float":
            widget = BoundedFloatText(value=value, max=bound_tmp[1],
                                      min=bound_tmp[0], step=bound_tmp[2])
        elif typestr == "list":
            widget = Text(value=json.dumps(value))
        elif typestr == "dict":
            widget = Textarea(value=yaml.dump(value))
        elif typestr == "file":
            layout = Layout(width='40em')  # 26em
            value_already_set = os.sep in str(value)
            widget = FileChooser(select_default=value_already_set)
            if value_already_set:
                widget.default_path = str(value).rsplit(os.sep, 1)[0]
                widget.default_filename = str(value).rsplit(os.sep, 1)[1]
                widget.reset()
        elif typestr == "integerrange":
            layout = Layout(width='40em')
            if not bounded:
                widget = IntRangeSlider(value=value)
            else:
                widget = IntRangeSlider(value=value, max=bound_tmp[1],
                                        min=bound_tmp[0], step=bound_tmp[2])
        elif typestr == 'floatrange':
            layout = Layout(width='40em')
            if not bounded:
                widget = FloatRangeSlider(value=value)
            else:
                widget = FloatRangeSlider(value=value, max=bound_tmp[1],
                                          min=bound_tmp[0], step=bound_tmp[2])
        else:
            widget = Text(value=str(value), disabled=True)
        widget.description = desc_label
        widget.description_tooltip = desc_tooltip
        widget.style = {'description_width': 'initial'}
        widget.layout = layout
        return widget

    def _on_module_settings_box_load(self, btn: DOMWidget) -> None:
        desc = self.module.get_module_desc()
        hide = desc.get('jupyter-gui-config-hide', [])
        type_overrides = desc.get('jupyter-gui-override-settings-type', {})
        for k, v in self._widgets.items():
            try:
                if v.disabled:  # don't set settings with unknown types
                    continue
            except AttributeError:  # some widgets can't be disabled
                pass  # they are always important
            # skip hidden elements
            if k in hide:
                continue
            if type(self.module.config[k]) not in type_2_str:
                print("Warn: Widget doesn't support type: %s (key: %s" % 
                        (type(self.module.config[k]), k))
            type_str = type_2_str[type(self.module.config[k])]
            if k in type_overrides:
                type_str = type_overrides[k]
            # Set based on type:
            if type_str == 'list':
                v.value = json.dumps(self.module.config[k])
            elif type_str == 'dict':
                v.value = yaml.dump(value)
            else:
                v.value = self.module.config[k]

    def _on_module_settings_box_save(self, btn: DOMWidget) -> None:
        any_error = False
        for k, v in self._widgets.items():
            try:
                if v.disabled:  # don't set settings with unknown types
                    continue
            except AttributeError:  # some widgets can't be disabled
                pass  # they are always important
            desc = self.module.get_module_desc()
            hide = desc.get('jupyter-gui-config-hide', [])
            # handle special types:
            # cklarhor: this is wrong!:
            type_overrides = desc.get('jupyter-gui-override-settings-type', {})
            if k in hide:
                continue
            if k in type_overrides:
                type_str = type_overrides[k]
            else:
                type_str = type_2_str[type(self.module.config[k])]
            if type_str == 'list':
                try:
                    self.module.update_config(k, json.loads(v.value))
                except json.JSONDecodeError as e:
                    self.settings_info.value = f"Warning not all settings could be saved!: Last error {k} -> {e}"
                    any_error = True
                except NoSuchChannel as e:
                    self.settings_info.value = f"Warning channel not found {k} -> {e}"
                    any_error = True
            elif type_str == 'dict':
                config = v.value
                if len(config) == 0:
                    config = '{}'
                try:
                    dict_item = yaml.safe_load(config)
                    v.layout = Layout()
                    self.module.update_config(k, dict_item)
                except yaml.parser.ParserError as e:
                    traceback.print_exc()
                    v.layout = Layout(border="1px solid red")
            else:  # handle easy types:
                self.module.update_config(k, v.value)
        if not any_error:
            self.settings_info.value = ""

    def _get_module_settings_box(self) -> DOMWidget:
        self._widgets: dict[str, DOMWidget] = {}
        setting_gui_groups: dict[str, DOMWidget] = {}
        for k, v in self.module.config.items():
            # Guess setting group based on prefix, store label
            if '_' in k:
                group = k.split('_', 1)[0]
                label = k.split('_', 1)[1]
            else:
                group = _('Unsorted')
                label = k
            group += ':'
            label += ':'
            # Guess setting widget based on current value
            md = self.module.get_module_desc()
            type_overrides = md.get('jupyter-gui-override-settings-type', {})
            if k in type_overrides:
                typestr = type_overrides[k]
            else:
                typestr = type_2_str.get(type(v), 'unknown')
            description = self.module.config_desc.get(k, '')
            widget = self._get_settingwidget_by_string(typestr, v, label,
                                                       description, k)
            self._widgets[k] = widget
            if k in md.get('jupyter-gui-config-hide', []):
                widget.style.text_color = 'grey'
            target_gui_group = setting_gui_groups.get(group, [])
            target_gui_group.append(widget)
            setting_gui_groups[group] = target_gui_group
            # gen main layout
            settings_boxes = []
            for k, v in setting_gui_groups.items():
                title = Label(k)
                box = VBox(children=[title]+v,
                           layout=Layout(border="solid 1px grey",
                                         # margin="0px 0px 0px -1px",
                                         padding="5px"))
                settings_boxes.append(box)
            settings_box = HBox(children=settings_boxes,
                                layout=Layout(display='flex',
                                              flex_flow='wrap'))
            settings_save = Button(description=_("Save"))
            settings_save.on_click(self._on_module_settings_box_save)
            settings_reset = Button(description=_("Refresh"))
            settings_reset.on_click(self._on_module_settings_box_load)
            self.settings_info = Label(value="")
            settings_bottom = HBox(children=[settings_save, settings_reset, self.settings_info])

        def toggle_expert_settings(state: dict):
            if not state['name'] == 'value':
                return
            md = self.module.get_module_desc()
            expert_list = md.get('jupyter-gui-config-hide', [])
            for settings_key, widget in self._widgets.items():
                if settings_key in expert_list:
                    widget.layout.visibility = 'visible' if state['new'] else \
                                               'hidden'
        show_expert_set = Checkbox(value=True,
                                   description=_("Show expert settings"))
        show_expert_set.observe(toggle_expert_settings)
        return VBox(children=[settings_box, show_expert_set, settings_bottom])

    def _get_module_pull_box(self) -> DOMWidget:
        pull_monitor = Checkbox(value=False,
                                description=_("Show inputs"))
        self.pull_box_pull_tabs = Tab()
        # TODO
        # push_monitor.observe(toggle_monitor)
        return VBox(children=[pull_monitor, self.pull_box_pull_tabs])

    def _get_module_push_box(self) -> DOMWidget:
        push_monitor = Checkbox(value=False,
                                description=_("Show outputs"))
        self.push_box_push_tabs = Tab()

        def new_push_pre_cb(channel: str, data: DataFrame) -> None:
            # limit rows & cols to the first 100
            num_cols = len(data.columns)
            num_rows = len(data)
            tmp_data = data.head(100)
            if num_cols > 100:
                tmp_data = tmp_data.iloc[:, :100]
                print(_("Warning your DF contains more than 100 (%d) cols, "
                       "the GUI will only show the first 100 cols"), num_cols)
            if num_rows>100:
                print(_("Warning your DF contains more than 100 (%d) rows, "
                       "the GUI will only show the first 100 rows"), num_rows)
            push_sheet = qgrid.show_grid(tmp_data, show_toolbar=True)
            # tabs have _title until jupyterwidget v8 after that it was renamed to title!
            print(getattr(self.push_box_push_tabs, 'titles', {}))
            title_test = getattr(self.push_box_push_tabs, 'titles', {})
            if channel in getattr(self.push_box_push_tabs, '_titles', {}).values() or (type(title_test) == tuple and channel in title_test) or (type(title_test) == dict and channel in title_test.values()):
                if hasattr(self.push_box_push_tabs, '_titles'): #old widgets
                    key = [k for k, v in self.push_box_push_tabs._titles.items()
                           if v == channel][0]
                else:
                    if type(title_test) == tuple:
                        key = title_test.index(channel)
                    else:
                        key = [k for k, v in self.push_box_push_tabs.titles.items() #new widgets
                               if v == channel][0]
                child_idx = int(key)-1
                childs_list = list(self.push_box_push_tabs.children)
                childs_list[child_idx] = push_sheet
                self.push_box_push_tabs.children = tuple(childs_list)
            else:
                self.push_box_push_tabs.children += (push_sheet, )
                num_childs = len(self.push_box_push_tabs.children)
                try:
                    self.push_box_push_tabs.set_title(num_childs-1, channel)
                except IndexError: #older? ipywidget hack
                    self.push_box_push_tabs.set_title(num_childs, channel)
                    #tmp = self.push_box_push_tabs.titles
                    #assert len(tmp)==num_childs-1, f"Error: {len(tmp)} vs {num_childs}"
                    #self.push_box_push_tabs.titles += (channel, )
                
            self._push_box_cb(channel, data)

        def toggle_monitor(state: dict):
            if not state['name'] == 'value':
                return
            status = state['new']
            if status:
                self._push_box_cb = self.module._push_data
                self.module._push_data = new_push_pre_cb
            else:
                self.module._push_data = self._push_box_cb
        push_monitor.observe(toggle_monitor)
        return VBox(children=[push_monitor, self.push_box_push_tabs])
