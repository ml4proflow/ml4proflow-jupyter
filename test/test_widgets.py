import unittest
from ml4proflow.modules import DataFlowManager, SourceModule
from ml4proflow.modules_extra import PassThroughModule
from ml4proflow.exceptions import InterfaceFunction
from ml4proflow_jupyter.exceptions import NoSuchWidget
from ml4proflow_jupyter import widget_finder, widgets


class TestWidgetFinder(unittest.TestCase):
    def test_WidgetFinder(self):
        widget_name = "ml4proflow_jupyter.widgets.BasicWidget"
        widget_cls = widget_finder.find_by_name(widget_name)
        self.assertTrue(issubclass(widget_cls, widgets.BasicWidget))

    def test_UnknownModule(self):
        with self.assertRaises(ModuleNotFoundError):
            widget_finder.find_by_name("ml4proflow_jupyter.no_such_module.B")

    def test_UnknownWidget(self):
        with self.assertRaises(NoSuchWidget):
            widget_finder.find_by_name("ml4proflow_jupyter.widgets.B")


class TestWidget(unittest.TestCase):
    def setUp(self):
        self.dfm = DataFlowManager()

    def test_InterfaceRaiseException(self):
        w = widgets.Widgetable(None)
        with self.assertRaises(InterfaceFunction):
            w.get_module_main_row_gui()

    def test_WidgetCreate(self):
        widget_name = "ml4proflow_jupyter.widgets.BasicWidget"
        widget_cls = widget_finder.find_by_name(widget_name)
        module = PassThroughModule(self.dfm, {})
        widget_ins = widget_cls(module)
        self.assertIsInstance(widget_ins, widgets.BasicWidget)

    def test_WidgetGetRowGui(self):
        widget_name = "ml4proflow_jupyter.widgets.BasicWidget"
        widget_cls = widget_finder.find_by_name(widget_name)
        module = PassThroughModule(self.dfm, {})
        widget_ins = widget_cls(module)
        widget_ins.get_module_main_row_gui()


class TestWidgetTypeConverting(unittest.TestCase):
    def setUp(self):
        self.dfm = DataFlowManager()

    def test_checkListConverting(self):
        int_list = [1, 2]
        float_list = [1.0, 2.0]
        str_list = ['1', '2']
        # TEST PREP
        dummy_m = SourceModule(self.dfm, {})
        dummy_m.config['int_list'] = int_list
        dummy_m.config['float_list'] = float_list
        dummy_m.config['str_list'] = str_list
        widget_name = "ml4proflow_jupyter.widgets.BasicWidget"
        widget_cls = widget_finder.find_by_name(widget_name)
        widget_ins = widget_cls(dummy_m)
        # START TEST: CREATE -> SAVE -> CHECK TYPES
        widget_ins.get_module_main_row_gui()
        widget_ins.get_module_main_gui()

        widget_ins._on_module_settings_box_save(None)
        # Check outer types
        self.assertIsInstance(dummy_m.config['int_list'], list)
        self.assertIsInstance(dummy_m.config['float_list'], list)
        self.assertIsInstance(dummy_m.config['str_list'], list)
        # Check inner types
        self.assertIsInstance(dummy_m.config['int_list'][0], int)
        self.assertIsInstance(dummy_m.config['int_list'][1], int)
        self.assertIsInstance(dummy_m.config['float_list'][0], float)
        self.assertIsInstance(dummy_m.config['float_list'][1], float)
        self.assertIsInstance(dummy_m.config['str_list'][0], str)
        self.assertIsInstance(dummy_m.config['str_list'][1], str)
        widget_ins._on_module_settings_box_load(None)
        widget_ins._on_module_settings_box_save(None)
        # Check outer types
        self.assertIsInstance(dummy_m.config['int_list'], list)
        self.assertIsInstance(dummy_m.config['float_list'], list)
        self.assertIsInstance(dummy_m.config['str_list'], list)
        # Check inner types
        self.assertIsInstance(dummy_m.config['int_list'][0], int)
        self.assertIsInstance(dummy_m.config['int_list'][1], int)
        self.assertIsInstance(dummy_m.config['float_list'][0], float)
        self.assertIsInstance(dummy_m.config['float_list'][1], float)
        self.assertIsInstance(dummy_m.config['str_list'][0], str)
        self.assertIsInstance(dummy_m.config['str_list'][1], str)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
