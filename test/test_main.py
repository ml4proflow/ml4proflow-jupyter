import unittest
from ml4proflow_jupyter import main
from ipywidgets import HBox


class TestMainWindow(unittest.TestCase):
    def test_create_DataFlowManager(self):
        dut = main.MainWindow()
        self.assertIsInstance(dut, HBox)

    def test_create_DataFlowManager_add(self):
        dut = main.MainWindow()
        dut.on_new_dfg_button(dut.gui_new_dfg_button)
        self.assertIsInstance(dut, HBox)

    def test_create_DataFlowManager_addflow_addmodule(self):
        dut = main.MainWindow()
        dut.on_new_dfg_button(dut.gui_new_dfg_button)
        dut.children[0].container_dfg.show_select_new_module_dialog()
        self.assertIsInstance(dut, HBox)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
